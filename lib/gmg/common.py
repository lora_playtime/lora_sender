import pycom
import time
import ubinascii
from network import LoRa, WLAN


class Colour(object):
    TRANSPARENT = 0x000000
    RED = 0x110000
    GREEN = 0x001100
    BLUE = 0x000011


def blink(col, rep, pause):
    for i in range(rep):
        pycom.rgbled(Colour.TRANSPARENT)
        time.sleep(pause)
        pycom.rgbled(col)
        time.sleep(pause)


def mac_wlan():
    return ubinascii.hexlify(WLAN().mac(), ':').decode()


def mac_lora():
    return ubinascii.hexlify(LoRa().mac(), ':').decode()
