from L76GNSS import L76GNSS  # GPS
from LIS2HH12 import LIS2HH12  # 3-Axis Accelerometer
from LTR329ALS01 import LTR329ALS01  # Ambient Light
from MPL3115A2 import MPL3115A2  # Barometric Pressure & Altitude
from SI7006A20 import SI7006A20  # Humidity & Temperature
from pytrack import Pytrack
from pysense import Pysense


def initialise_sensor_board(board_type):
    SensorBoard.set_sensor_board_type(board_type)
    
    if board_type == SensorBoard.PYTRACK:
        return PyTrack()
    elif board_type == SensorBoard.PYSENSE:
        return PySense()
    else:
        return None


class SensorBoard(object):
    PYTRACK = 0
    PYSENSE = 1
    board = None
    board_type = None

    def __init__(self):
        if not (self.has_pytrack() or self.has_pysense()):
            raise RuntimeError('Must initialise sensor type!')
        # TODO: find a way automatically to check which board is connected

    @classmethod
    def set_sensor_board_type(cls, board_type):
        if cls.has_pytrack() or cls.has_pysense():
            raise RuntimeError('Cannot change the type of board after initialisation')
        
        if board_type == cls.PYTRACK:
            cls.board = Pytrack()
        elif board_type == cls.PYSENSE:
            cls.board = Pysense()
        else:
            raise ValueError('Invalid sensor board type given')
        cls.board_type = board_type
    
    @classmethod
    def has_pytrack(cls):
        return cls.board_type == cls.PYTRACK

    @classmethod
    def has_pysense(cls):
        return cls.board_type == cls.PYSENSE


class PyTrack(SensorBoard, LIS2HH12, L76GNSS):
    def __init__(self):
        SensorBoard.__init__(self)
        if not self.has_pytrack():
            raise RuntimeError('Cannot use PyTrack if pytrack board is not in use')
        
        LIS2HH12.__init__(self, self.board)#, **accelerometer_args)
        L76GNSS.__init__(self, self.board)#, **gps_args)


class PySense(SensorBoard, LIS2HH12, SI7006A20, MPL3115A2, LTR329ALS01):

    def __init__(self):
        SensorBoard.__init__(self)
        if not self.has_pysense():
            raise RuntimeError('Cannot use PySense if pysense board is not in use')
        
        LIS2HH12.__init__(self, self.board)
        SI7006A20.__init__(self, self.board)
        MPL3115A2.__init__(self, self.board) # mode defaults to PRESSURE
        LTR329ALS01.__init__(self, self.board)

    def temperature(self):
        return SI7006A20.temperature(self)
