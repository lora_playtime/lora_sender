from network import LoRa
from LIS2HH12 import LIS2HH12
from pysense import Pysense
#from gmg.sensors import initialise_sensor_board, SensorBoard
import socket
import machine
import time


# initialize LoRa in LORA mode
# more params can also be given, like frequency, tx power and spreading factor
lora = LoRa(mode=LoRa.LORA)

py = Pysense()
acc = LIS2HH12()

# create a raw LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
#pysense = initialise_sensor_board(SensorBoard.PYSENSE)
while True:
    #Collect sensor data
    pitch = acc.pitch()
    roll = acc.roll()
    accel = acc.acceleration()
    print('Hello world from 6AE0 \n ')
    print ('Welcome to IoT Meeting \n ')
    print('{},{},{}'.format(pitch,roll,accel))
    # send some data
    s.setblocking(True)
    s.send('Hello from 6AE0: Your data is: {},{}'.format(pitch,roll))

    # get any data received...
    #s.setblocking(False)
    #data = s.recv(64)
    #print("looping")

    # wait a random amount of time
    time.sleep(machine.rng() & 0x0F)
