import machine
from network import WLAN
import pycom
import time

from gmg.common import mac_wlan


# set up wifi AP
if machine.reset_cause() != machine.SOFT_RESET:
    ssid_suffix = ''.join(mac_wlan().split(':')[-2:])
    wlan = WLAN(mode=WLAN.AP)
    wlan.init(mode=WLAN.AP, ssid='lopy-wlan-%s' % ssid_suffix, auth=(WLAN.WPA2, 'www.pycom.io'))

#pycom.heartbeat(False)  # disable LED heartbeat
time.sleep(5)
